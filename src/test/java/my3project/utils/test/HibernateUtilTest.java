package my3project.utils.test;

import grupe3.HibernateApplication.utilities.HibernateUtilities;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

// User HSQLDB for in-memory DB testing
// See https://examples.javacodegeeks.com/core-java/junit/junit-hsqldb-example/

public class HibernateUtilTest {

    private static SessionFactory sessionFactory;
    private Session session;

    @BeforeAll
    public static void setup() throws SQLException, ClassNotFoundException, IOException {

        // Check whether such class can be loaded at all in runtime
        assertNotNull( Class.forName("org.hsqldb.jdbc.JDBCDriver") );
        sessionFactory = HibernateUtilities.getSessionFactory();

        assertNotNull( sessionFactory, "Session factory initialization fails" );
        assertFalse( sessionFactory.isClosed(), "Session cannot be opened" );
        System.out.println("SessionFactory created");
    }

    @AfterAll
    public static void tearDown() {
        if (sessionFactory != null) sessionFactory.close();
        System.out.println("SessionFactory destroyed");
    }

    @Test
    public void testCreate() {
    }

    @Test
    public void testUpdate() {
    }

    @Test
    public void testGet() {
    }

    @Test
    public void testList() {
    }

    @Test
    public void testDelete() {
    }

    @BeforeEach
    public void openSession() {
        session = sessionFactory.openSession();
        System.out.println("Session created");
    }

    @AfterEach
    public void closeSession() {
        if (session != null) session.close();
        System.out.println("Session closed\n");
    }
}
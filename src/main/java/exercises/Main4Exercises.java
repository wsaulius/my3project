package exercises;

import java.sql.SQLException;

public class Main4Exercises {

    public static void main(String[] args){

        System.out.println("------------SELECT * operation-------------");
        try {
            SelectAllExerciseDBProjects.queryOperation();
        } catch (SQLException e) {
            System.out.println("Query was failed: " + e.getMessage());
        }

    }
}


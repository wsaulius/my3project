package example;

import example.CommandOperation;
import example.PreparedstatementOperation;
import example.QueryOperation;
import example.TransactionOperation;


/**
 * An application for demo of Java and Hibernate persistence
 * <p>
 * It does implement {@link #queryOperation(example.QueryOperation) queryOperation} for queries
 * Please visit a link to the interface's documentation.
 */

public class Main4Examples {

    public static void main(String[] args){

        System.out.println("------------Query operation-------------");
        QueryOperation.queryOperation();

        System.out.println("------------Command operation-----------");
        CommandOperation.commandOperation();

        System.out.println("------------Prepared statement-----------");
        new PreparedstatementOperation().preparedstatementOperation();

        System.out.println("------------Transaction-----------------");
        TransactionOperation.transactionOperation();

    }
}

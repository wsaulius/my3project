package grupe3.HibernateApplication.access;

import grupe3.HibernateApplication.entities.Project;

public interface IProjectCommands {

    Project findById(final Integer id);

    void save(Project project);
    void delete(Project project);
    void update(Project project);

}

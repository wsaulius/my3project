package grupe3.HibernateApplication.access;

import grupe3.HibernateApplication.entities.Project;
import grupe3.HibernateApplication.utilities.HibernateUtilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import java.sql.SQLException;

public class ProjectRepository extends HibernateUtilities implements IProjectCommands {

    Logger logger = LogManager.getLogger(ProjectRepository.class);

    public ProjectRepository queryOperation() throws SQLException {
        try {
            Project project = new Project();
            project.setDescription( "C# Project");
            save(project);

        } catch (Exception e){
            e.printStackTrace();

        }
        return this;
    }

    @Override
    public Project findById(Integer id) {
        return new Project();
    }

    @Override
    public void save(Project project) {
        transaction = null;
        try{
            Session session = HibernateUtilities.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.persist(project);
            transaction.commit();

        }catch (Exception e) {

            e.printStackTrace();
            transaction.rollback();

        }
    }

    @Override
    public void delete(Project project) {

    }

    @Override
    public void update(Project project) {

    }
}

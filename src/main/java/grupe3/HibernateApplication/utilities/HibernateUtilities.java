package grupe3.HibernateApplication.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class HibernateUtilities {

    private static Logger logger = LogManager.getLogger(HibernateUtilities.class);

    private static SessionFactory sessionFactory;
    protected Transaction transaction;

    public static SessionFactory getSessionFactory(){
        if (sessionFactory==null){
            try{
                Configuration configuration = new Configuration();
                configuration.configure("/hibernate.cfg.xml");
                sessionFactory = configuration.buildSessionFactory();

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}

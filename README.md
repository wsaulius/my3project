  
   Practical Project 
   ==========
   
   
     
  https://slides.java.en.sdacademy.pro/
  
  https://bluejeans.com/826392891/3602 


  
   Comfort/Curious balance:                                                                                                      
   
   
           * Skills: 
	   
	   - DDL - diagram, sugeneruoti SQL entities PK, FK 
	   - Hibernate configuration </xml> @Entity 
	   - JUnit5 testing java code 
	   - CRUD 
	   - Class Design / Patterns / DRY / SOLID / KISS 
	   - Hibernate JPA 
	   
	   * Entities / POJOs (plain old java objects) 
	   * DDL -data definition 
	   * ERD - entity relation diagram 	  
	   * ORM - object relational mapping / Hibernate annotations 
	   * DP - design patterns thinking 
	   * Trello tasks 
	      

 (1)   Finansinio rastingumo programele/FIRE app         (50% 30 % 20%)     4stars    Rokas, Giedre, Ina  (wsaulius) 
          
	- Pajamos, Islaidos :: (doSmth),
	- Pomegiai/laisvalaikis,  :: HobbyTime.assignAmount( BigInteger bg )
	- Taupymas/investavimas 
	- Biudzetas
	  
 
 (2)   1-2   Co-working app                                                            Vytautas, Kristijonas, Greta, Tomas   (wsaulius)
 
          -  Pastatas/Erdve ( n: vietuSkaicius, adresas, pavadinimas, ID, TV/projektorius ) 
          -  Adresas   
          -  Rezervacijos
          -  Klientas
	   
	   3-5 // entities 4practice project
	   6-8 // entities 4final project 
	   
		   
 (3)   1-3   Sub-rangovu app / (remonto registravimas, CRM)    
      (atrankos, užklausos, palyginimo sritys, pasiūlymai) -             Ugnė, Andrius, Sandra, Kipras  (wsaulius)
      
	- Tiekejas (veiklos srities kodas)  
	- Sritis (su kodu) + charakteristikos (elektros, varikliu, mechanines) 
	- Detales/Medziagos + charakteristikos 
	- Vartotojai  
	   

   Architecture:  Entity abstractions  and services via interfacing
   
   
   DELIVERY plan:
   ----------------------
   

     * Hibernate (annotation) configuration ASAP 
     * Entities in java 
     * Introduce FK relations in DB, then in Hibernate config 
     * One-to-many (minimum 1 occurrence) 
     
     * Create Repos or Business logic for DB CRUD operations 
     * Minumum user interaction menu (terminal window Scanners for input)  ::  1) [Create] 2) [Update] 3) [Delete]
     * Tests: persistence (CRUD) check the result in DB 

     * (optional) JavaDoc 
     * (optionally optional) H2 DB in memory configuration 
     * CI/CD runs 
     
     
 IMPLEMENTATION plan:
 ---------------------------------
 
 	1. Identify the POJOs that have a database representation. (restrict to one-to-many) 
	2. Identify which properties of those POJOs need to be persisted. (define fields in tables) 
	3. Annotate each of the POJOs to map your Java object's properties to columns in a database
	table (add annotations in @Entity POJOs) 
	4. Create the database schema using the schema export tool, use an existing database, or
	create your own database schema. (Produce a physical DB from POJOs in Java) 
	
	5. Add the Hibernate Java libraries to your application's classpath (maven).     
	6. Dump current DB state to project .sql (to be re-used) 
	
	7. Add Hibernate repositories code (CRUD) 
 	8. Add Services code 
	9. Add Tests 
	10. Update CI/CD
	
 
  MEETINGS: 
  ---------------
  
  * Checkup meeting on 11th 19:30 (GROUP2) 
		
  * Checkup meeting on 12th 19:30 (GROUP3) 
		
		
 
 --- optional --- 
 4)   Dokumentu administravimas, pasiekiamumas CRM 
	
 
 4)   *Humana (ADD on: sandeliavimas)  - rezervacijos                                                 * 


 TDD (test-driven development)
 
  master :: CI/CD  -->> checkout & build (jar/war/ear) --> < container/microservices >  ---> {{{ heroku / aws / azure }}} 
  
   -- loose coupling  and interfacing 
   
   -- gitflow  https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
   
 
 